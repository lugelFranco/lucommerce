import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { Compra } from './compra.entity';
import { Producto } from './producto.entity';

@Entity()
export class ProductoAgregado {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  cantidad: number;

  @ManyToOne(() => Compra, (c) => c.productosAgregados, { nullable: false })
  compra: Compra;

  @ManyToOne(() => Producto, (p) => p.productosAgregados, { nullable: false })
  producto: Producto;
}
