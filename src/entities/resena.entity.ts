import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { Producto } from './producto.entity';
import { Cliente } from './cliente.entity';

@Entity()
export class Resena {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  mensaje: string;

  @Column()
  calificacion: number;

  @ManyToOne(() => Producto, (p) => p.resenas, { nullable: false })
  producto: Producto;

  @ManyToOne(() => Cliente, (c) => c.resenas, { nullable: false })
  cliente: Cliente;
}
