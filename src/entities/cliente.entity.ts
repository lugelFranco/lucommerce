import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { Compra } from './compra.entity';
import { Favorito } from './favorito.entity';
import { Resena } from './resena.entity';

@Entity()
export class Cliente {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  nombre: string;

  @Column()
  apellido: string;

  @Column()
  celular: string;
  
  @Column()
  email: string;

  @OneToMany(() => Compra, (c) => c.cliente)
  compras: Compra[];

  @OneToMany(() => Favorito, (c) => c.cliente)
  favoritos: Favorito[];

  @OneToMany(() => Resena, (f) => f.cliente)
  resenas: Resena[];
}
