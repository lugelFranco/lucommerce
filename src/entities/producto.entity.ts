import {
  Entity,
  OneToMany,
  PrimaryGeneratedColumn,
  ManyToOne,
  Column,
} from 'typeorm';
import { Compra } from './compra.entity';
import { Vendedor } from './vendedor.entity';
import { Favorito } from './favorito.entity';
import { Resena } from './resena.entity';
import { ProductoAgregado } from './producto-agregado.entity';
import { Categorias } from 'src/common/enums';

@Entity()
export class Producto {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  nombre: string;

  @Column()
  valor: number;

  @Column()
  categoria: Categorias;

  @Column({nullable: true, type:'longtext'})
  logo: string;

  @ManyToOne(() => Vendedor, (v) => v.productos, { nullable: false })
  vendedor: Vendedor;

  @OneToMany(() => Favorito, (f) => f.producto)
  favoritos: Favorito[];

  @OneToMany(() => Resena, (f) => f.producto)
  resenas: Resena[];

  @OneToMany(() => ProductoAgregado, (pa) => pa.producto)
  productosAgregados: ProductoAgregado[];
}
