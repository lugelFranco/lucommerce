import { Categorias } from 'src/common/enums';
import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { Producto } from './producto.entity';

@Entity()
export class Vendedor {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  nit: number;

  @Column()
  nombre: string;

  @Column()
  email: string;

  @Column()
  categoria: Categorias;

  @OneToMany(() => Producto, (p) => p.vendedor)
  productos: Producto[];
}
