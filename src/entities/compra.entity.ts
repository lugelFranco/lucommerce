import { Column, Entity, ManyToOne, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { Cliente } from './cliente.entity';
import { ProductoAgregado } from './producto-agregado.entity';

@Entity({})
export class Compra {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  fecha: Date;

  @Column()
  finalizada: boolean;

  @ManyToOne(() => Cliente, (c) => c.compras, {
    nullable: false,
  })
  cliente: Cliente;
  
  @OneToMany(() => ProductoAgregado, (pa) => pa.compra)
  productosAgregados: ProductoAgregado;
}
