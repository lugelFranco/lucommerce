import { Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { Producto } from './producto.entity';
import { Cliente } from './cliente.entity';

@Entity()
export class Favorito {
  @PrimaryGeneratedColumn()
  id: number;

  @ManyToOne(() => Producto, (p) => p.favoritos, { nullable: false })
  producto: Producto;

  @ManyToOne(() => Cliente, (c) => c.favoritos, { nullable: false })
  cliente: Cliente;
}
