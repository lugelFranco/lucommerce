import {
  Controller,
  Delete,
  Get,
  HttpStatus,
  Param,
  Post,
  Put,
  Req,
  Res,
} from '@nestjs/common';
import { Request, Response } from 'express';
import { ProductoService } from 'src/services/producto.service';

@Controller('producto')
export class ProductoController {
  constructor(private productoService: ProductoService) {}

  @Post()
  crearProducto(@Res() response: Response, @Req() request: Request) {
    console.log(request.body);
    this.productoService
      .crearProducto(request.body)
      .then((res) => {
        console.log(res);
        response.status(HttpStatus.CREATED).json(res);
      })
      .catch((err) => {
        console.log(err);
        response.status(HttpStatus.INTERNAL_SERVER_ERROR).json(err);
      });
  }

  @Get()
  listarProductos(@Res() response: Response) {
    this.productoService
      .listarProductos()
      .then((res) => {
        console.log(res);
        response.status(HttpStatus.OK).json(res);
      })
      .catch((err) => {
        console.log(err);
        response.status(HttpStatus.INTERNAL_SERVER_ERROR).json(err);
      });
  }

  @Delete('/:id')
  eliminarProducto(@Res() response: Response, @Param('id') id: number) {
    console.log('id', id);

    this.productoService
      .eliminarProducto(id)
      .then((res) => {
        console.log(res);
        response.status(HttpStatus.OK).json(res);
      })
      .catch((err) => {
        console.log(err);
        response.status(HttpStatus.INTERNAL_SERVER_ERROR).json(err);
      });
  }

  @Put('/:id')
  actualizarProducto(
    @Res() response: Response,
    @Req() request: Request,
    @Param('id') id: number,
  ) {
    this.productoService
      .actualizarProducto(id, request.body)
      .then((res) => {
        response.status(HttpStatus.OK).json(res);
      })
      .catch((err) => {
        console.log(err);
        response.status(HttpStatus.INTERNAL_SERVER_ERROR).json(err);
      });
  }
}
