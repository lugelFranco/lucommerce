import {
  Controller,
  Delete,
  Get,
  HttpStatus,
  Param,
  Post,
  Put,
  Req,
  Res,
} from '@nestjs/common';
import { Request, Response } from 'express';
import { CompraService } from 'src/services/compra.service';

@Controller('compra')
export class CompraController {
  constructor(private compraService: CompraService) {}

  @Post()
  crearCompra(@Res() response: Response, @Req() request: Request) {
    console.log(request.body);
    this.compraService
      .crearCompra(request.body)
      .then((res) => {
        console.log(res);
        response.status(HttpStatus.CREATED).json(res);
      })
      .catch((err) => {
        console.log(err);
        response.status(HttpStatus.INTERNAL_SERVER_ERROR).json(err);
      });
  }

  @Get()
  listarCompras(@Res() response: Response) {
    this.compraService
      .listarCompras()
      .then((res) => {
        console.log(res);
        response.status(HttpStatus.OK).json(res);
      })
      .catch((err) => {
        console.log(err);
        response.status(HttpStatus.INTERNAL_SERVER_ERROR).json(err);
      });
  }

  @Delete('/:id')
  eliminarCompra(@Res() response: Response, @Param('id') id: number) {
    console.log('id', id);

    this.compraService
      .eliminarCompra(id)
      .then((res) => {
        console.log(res);
        response.status(HttpStatus.OK).json(res);
      })
      .catch((err) => {
        console.log(err);
        response.status(HttpStatus.INTERNAL_SERVER_ERROR).json(err);
      });
  }

  @Put('/:id')
  actualizarCompra(
    @Res() response: Response,
    @Req() request: Request,
    @Param('id') id: number,
  ) {
    this.compraService
      .actualizarCompra(id, request.body)
      .then((res) => {
        response.status(HttpStatus.OK).json(res);
      })
      .catch((err) => {
        console.log(err);
        response.status(HttpStatus.INTERNAL_SERVER_ERROR).json(err);
      });
  }

  @Get('/:id')
  verCompraActual(
    @Res() response: Response,
    @Param('id') id: number){
    
    this.compraService
      .verCompraActual(id)
      .then((res) => {
        response.status(HttpStatus.OK).json(res);
      })
      .catch((err) => {
        console.log(err);
        response.status(HttpStatus.INTERNAL_SERVER_ERROR).json(err);
      });

    }
}
