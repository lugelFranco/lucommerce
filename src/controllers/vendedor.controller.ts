import {
  Controller,
  Delete,
  Get,
  HttpStatus,
  Param,
  Post,
  Put,
  Req,
  Res,
} from '@nestjs/common';
import { Request, Response } from 'express';
import { VendedorService } from 'src/services/vendedor.service';

@Controller('vendedor')
export class VendedorController {
  constructor(private vendedorService: VendedorService) {}

  @Post()
  crearVendedor(@Res() response: Response, @Req() request: Request) {
    console.log(request.body);
    this.vendedorService
      .crearVendedor(request.body)
      .then((res) => {
        console.log(res);
        response.status(HttpStatus.CREATED).json(res);
      })
      .catch((err) => {
        console.log(err);
        response.status(HttpStatus.INTERNAL_SERVER_ERROR).json(err);
      });
  }

  @Get()
  listarVendedors(@Res() response: Response) {
    this.vendedorService
      .listarVendedors()
      .then((res) => {
        console.log(res);
        response.status(HttpStatus.OK).json(res);
      })
      .catch((err) => {
        console.log(err);
        response.status(HttpStatus.INTERNAL_SERVER_ERROR).json(err);
      });
  }

  @Delete('/:id')
  eliminarVendedor(@Res() response: Response, @Param('id') id: number) {
    console.log('id', id);

    this.vendedorService
      .eliminarVendedor(id)
      .then((res) => {
        console.log(res);
        response.status(HttpStatus.OK).json(res);
      })
      .catch((err) => {
        console.log(err);
        response.status(HttpStatus.INTERNAL_SERVER_ERROR).json(err);
      });
  }

  @Put('/:id')
  actualizarVendedor(
    @Res() response: Response,
    @Req() request: Request,
    @Param('id') id: number,
  ) {
    this.vendedorService
      .actualizarVendedor(id, request.body)
      .then((res) => {
        response.status(HttpStatus.OK).json(res);
      })
      .catch((err) => {
        console.log(err);
        response.status(HttpStatus.INTERNAL_SERVER_ERROR).json(err);
      });
  }
}
