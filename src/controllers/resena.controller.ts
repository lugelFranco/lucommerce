import {
  Controller,
  Delete,
  Get,
  HttpStatus,
  Param,
  Post,
  Put,
  Req,
  Res,
} from '@nestjs/common';
import { Request, Response } from 'express';
import { ResenaService } from 'src/services/resena.service';

@Controller('resena')
export class ResenaController {
  constructor(private resenaService: ResenaService) {}

  @Post()
  crearResena(@Res() response: Response, @Req() request: Request) {
    console.log(request.body);
    this.resenaService
      .crearResena(request.body)
      .then((res) => {
        console.log(res);
        response.status(HttpStatus.CREATED).json(res);
      })
      .catch((err) => {
        console.log(err);
        response.status(HttpStatus.INTERNAL_SERVER_ERROR).json(err);
      });
  }

  @Get()
  listarResenas(@Res() response: Response) {
    this.resenaService
      .listarResenas()
      .then((res) => {
        console.log(res);
        response.status(HttpStatus.OK).json(res);
      })
      .catch((err) => {
        console.log(err);
        response.status(HttpStatus.INTERNAL_SERVER_ERROR).json(err);
      });
  }

  @Delete('/:id')
  eliminarResena(@Res() response: Response, @Param('id') id: number) {
    console.log('id', id);

    this.resenaService
      .eliminarResena(id)
      .then((res) => {
        console.log(res);
        response.status(HttpStatus.OK).json(res);
      })
      .catch((err) => {
        console.log(err);
        response.status(HttpStatus.INTERNAL_SERVER_ERROR).json(err);
      });
  }

  @Put('/:id')
  actualizarResena(
    @Res() response: Response,
    @Req() request: Request,
    @Param('id') id: number,
  ) {
    this.resenaService
      .actualizarResena(id, request.body)
      .then((res) => {
        response.status(HttpStatus.OK).json(res);
      })
      .catch((err) => {
        console.log(err);
        response.status(HttpStatus.INTERNAL_SERVER_ERROR).json(err);
      });
  }
}
