import {
  Controller,
  Delete,
  Get,
  HttpStatus,
  Param,
  Post,
  Put,
  Req,
  Res,
} from '@nestjs/common';
import { Request, Response } from 'express';
import { ProductoAgregadoService } from 'src/services/producto-agregado.service';

@Controller('producto-agregado')
export class ProductoAgregadoController {
  constructor(private productoAgregadoService: ProductoAgregadoService) {}

  @Post()
  crearProductoAgregado(@Res() response: Response, @Req() request: Request) {
    console.log(request.body);
    this.productoAgregadoService
      .crearProductoAgregado(request.body)
      .then((res) => {
        console.log(res);
        response.status(HttpStatus.CREATED).json(res);
      })
      .catch((err) => {
        console.log(err);
        response.status(HttpStatus.INTERNAL_SERVER_ERROR).json(err);
      });
  }

  @Get()
  listarProductoAgregados(@Res() response: Response) {
    this.productoAgregadoService
      .listarProductoAgregados()
      .then((res) => {
        console.log(res);
        response.status(HttpStatus.OK).json(res);
      })
      .catch((err) => {
        console.log(err);
        response.status(HttpStatus.INTERNAL_SERVER_ERROR).json(err);
      });
  }

  @Delete('/:id')
  eliminarProductoAgregado(@Res() response: Response, @Param('id') id: number) {
    console.log('id', id);

    this.productoAgregadoService
      .eliminarProductoAgregado(id)
      .then((res) => {
        console.log(res);
        response.status(HttpStatus.OK).json(res);
      })
      .catch((err) => {
        console.log(err);
        response.status(HttpStatus.INTERNAL_SERVER_ERROR).json(err);
      });
  }

  @Put('/:id')
  actualizarProductoAgregado(
    @Res() response: Response,
    @Req() request: Request,
    @Param('id') id: number,
  ) {
    this.productoAgregadoService
      .actualizarProductoAgregado(id, request.body)
      .then((res) => {
        response.status(HttpStatus.OK).json(res);
      })
      .catch((err) => {
        console.log(err);
        response.status(HttpStatus.INTERNAL_SERVER_ERROR).json(err);
      });
  }
}
