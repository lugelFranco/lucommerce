import {
  Controller,
  Delete,
  Get,
  HttpStatus,
  Param,
  Post,
  Put,
  Req,
  Res,
} from '@nestjs/common';
import { Request, Response } from 'express';
import { FavoritoService } from 'src/services/favorito.service';

@Controller('favorito')
export class FavoritoController {
  constructor(private favoritoService: FavoritoService) {}

  @Post()
  crearFavorito(@Res() response: Response, @Req() request: Request) {
    console.log(request.body);
    this.favoritoService
      .crearFavorito(request.body)
      .then((res) => {
        console.log(res);
        response.status(HttpStatus.CREATED).json(res);
      })
      .catch((err) => {
        console.log(err);
        response.status(HttpStatus.INTERNAL_SERVER_ERROR).json(err);
      });
  }

  @Get()
  listarFavoritos(@Res() response: Response) {
    this.favoritoService
      .listarFavoritos()
      .then((res) => {
        console.log(res);
        response.status(HttpStatus.OK).json(res);
      })
      .catch((err) => {
        console.log(err);
        response.status(HttpStatus.INTERNAL_SERVER_ERROR).json(err);
      });
  }

  @Delete('/:id')
  eliminarFavorito(@Res() response: Response, @Param('id') id: number) {
    console.log('id', id);

    this.favoritoService
      .eliminarFavorito(id)
      .then((res) => {
        console.log(res);
        response.status(HttpStatus.OK).json(res);
      })
      .catch((err) => {
        console.log(err);
        response.status(HttpStatus.INTERNAL_SERVER_ERROR).json(err);
      });
  }

  @Put('/:id')
  actualizarFavorito(
    @Res() response: Response,
    @Req() request: Request,
    @Param('id') id: number,
  ) {
    this.favoritoService
      .actualizarFavorito(id, request.body)
      .then((res) => {
        response.status(HttpStatus.OK).json(res);
      })
      .catch((err) => {
        console.log(err);
        response.status(HttpStatus.INTERNAL_SERVER_ERROR).json(err);
      });
  }
}
