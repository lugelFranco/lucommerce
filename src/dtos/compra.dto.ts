export class CompraDTO {
  id: number;
  fecha: Date;
  finalizada: boolean;
  clienteId: number;
}
