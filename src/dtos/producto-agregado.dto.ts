export class ProductoAgregadoDTO {
  id: number;
  cantidad: number;
  compraId?: number;
  productoId?: number;
}
