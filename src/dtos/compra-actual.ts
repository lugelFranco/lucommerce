import { ProductoAgregadoDTO } from "./producto-agregado.dto";

export class CompraActualDTO {
    totalItems: number;
    totalCompra: number;
    productosAgregados: ProductoAgregadoDTO[];
}