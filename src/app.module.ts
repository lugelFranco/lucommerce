import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { ClienteController } from './controllers/cliente.controller';
import { Cliente } from './entities/cliente.entity';
import { Compra } from './entities/compra.entity';
import { Favorito } from './entities/favorito.entity';
import { ProductoAgregado } from './entities/producto-agregado.entity';
import { Producto } from './entities/producto.entity';
import { Resena } from './entities/resena.entity';
import { Vendedor } from './entities/vendedor.entity';
import { ClienteService } from './services/cliente.service';
import { CompraController } from './controllers/compra.controller';
import { FavoritoController } from './controllers/favorito.controller';
import { ProductoAgregadoController } from './controllers/producto-agregado.controller';
import { ProductoController } from './controllers/producto.controller';
import { ResenaController } from './controllers/resena.controller';
import { VendedorController } from './controllers/vendedor.controller';
import { CompraService } from './services/compra.service';
import { FavoritoService } from './services/favorito.service';
import { ProductoAgregadoService } from './services/producto-agregado.service';
import { ProductoService } from './services/producto.service';
import { ResenaService } from './services/resena.service';
import { VendedorService } from './services/vendedor.service';

const entities = [
  Cliente,
  Compra,
  Producto,
  Favorito,
  ProductoAgregado,
  Resena,
  Vendedor,
];
@Module({
  imports: [
    TypeOrmModule.forRoot({
      type: 'mysql',
      host: 'localhost',
      port: 3306,
      username: 'root',
      password: '',
      database: 'lucommerce',
      entities: entities,
      synchronize: true,
    }),
    TypeOrmModule.forFeature(entities),
  ],
  controllers: [
    AppController,
    ClienteController,
    CompraController,
    FavoritoController,
    ProductoAgregadoController,
    ProductoController,
    ResenaController,
    VendedorController,
  ],
  providers: [
    AppService,
    ClienteService,
    CompraService,
    FavoritoService,
    ProductoAgregadoService,
    ProductoService,
    ResenaService,
    VendedorService,
  ],
})
export class AppModule {}
