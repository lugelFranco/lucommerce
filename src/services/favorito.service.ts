import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { FavoritoDTO } from 'src/dtos/favorito.dto';
import { Repository } from 'typeorm';
import { Favorito } from '../entities/favorito.entity';

@Injectable()
export class FavoritoService {
  constructor(
    @InjectRepository(Favorito)
    private favoritoRepository: Repository<Favorito>,
  ) { }

  crearFavorito(favorito: FavoritoDTO): Promise<Favorito> {
    return this.favoritoRepository.save({
      ...favorito,
      cliente: { id: favorito.clienteId },
      producto: { id: favorito.productoId }
    });
  }

  listarFavoritos() {
    return this.favoritoRepository.find({relations: ['cliente', 'producto']});
  }

  eliminarFavorito(id: number) {
    return this.favoritoRepository.delete(id);
  }

  actualizarFavorito(id: number, favorito: FavoritoDTO) {
    return this.favoritoRepository.update(id, favorito);
  }
}
