import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { CompraActualDTO } from 'src/dtos/compra-actual';
import { CompraDTO } from 'src/dtos/compra.dto';
import { ProductoAgregado } from 'src/entities/producto-agregado.entity';
import { Repository } from 'typeorm';
import { Compra } from '../entities/compra.entity';

@Injectable()
export class CompraService {
  constructor(
    @InjectRepository(Compra)
    private compraRepository: Repository<Compra>,
    @InjectRepository(ProductoAgregado)
    private productoAagregadoRepository: Repository<ProductoAgregado>
  ) { }

  async verCompraActual(clienteId): Promise<CompraActualDTO> {
    const compraActual = await this.compraRepository.findOne({
      where: {
        cliente: {id: clienteId},
        fecha: null
      }
    });

    const productosAgregados = await this.productoAagregadoRepository.find(
      {
        where: {
          compra: compraActual
        },
        relations: ['producto']
      }
    );
    console.log(productosAgregados
      .map(p => {
        console.log(`${p.producto.nombre}: ${p.producto.valor}, el valor en la compra es ${ p.producto.valor * p.cantidad}`);
        
        return p.producto.valor * p.cantidad
      }));
    

    let totalCompra = 0;
    totalCompra = productosAgregados
      .map(p => p.producto.valor * p.cantidad)
      .reduce((total, p) => {
        console.log(`El acumulado es: ${total} y se acumula ${p}`);
        
        return p + total
      });

    return {
      productosAgregados,
      totalCompra,
      totalItems: productosAgregados.length
    }
  }

  crearCompra(compra: CompraDTO): Promise<Compra> {
    return this.compraRepository.save({
      ...compra,
      cliente: { id: compra.clienteId }
    });
  }

  listarCompras() {
    return this.compraRepository.find();
  }

  eliminarCompra(id: number) {
    return this.compraRepository.delete(id);
  }

  actualizarCompra(id: number, compra: CompraDTO) {
    return this.compraRepository.update(id, compra);
  }
}
