import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { VendedorDTO } from 'src/dtos/vendedor.dto';
import { Repository } from 'typeorm';
import { Vendedor } from '../entities/vendedor.entity';

@Injectable()
export class VendedorService {
  constructor(
    @InjectRepository(Vendedor)
    private vendedorRepository: Repository<Vendedor>,
  ) {}

  crearVendedor(vendedor: VendedorDTO): Promise<Vendedor> {
    return this.vendedorRepository.save(vendedor);
  }

  listarVendedors() {
    return this.vendedorRepository.find();
  }

  eliminarVendedor(id: number) {
    return this.vendedorRepository.delete(id);
  }

  actualizarVendedor(id: number, vendedor: VendedorDTO) {
    return this.vendedorRepository.update(id, vendedor);
  }
}
