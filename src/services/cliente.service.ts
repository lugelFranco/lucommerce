import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { ClienteDTO } from 'src/dtos/cliente.dto';
import { Repository } from 'typeorm';
import { Cliente } from '../entities/cliente.entity';

@Injectable()
export class ClienteService {
  constructor(
    @InjectRepository(Cliente)
    private clienteRepository: Repository<Cliente>,
  ) {}

  crearCliente(cliente: ClienteDTO): Promise<Cliente> {
    return this.clienteRepository.save(cliente);
  }

  listarClientes() {
    return this.clienteRepository.find();
  }

  eliminarCliente(id: number) {
    return this.clienteRepository.delete(id);
  }

  actualizarCliente(id: number, cliente: ClienteDTO) {
    return this.clienteRepository.update(id, cliente);
  }
}
