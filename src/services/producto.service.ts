import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { ProductoDTO } from 'src/dtos/producto.dto';
import { Repository } from 'typeorm';
import { Producto } from '../entities/producto.entity';

@Injectable()
export class ProductoService {
  constructor(
    @InjectRepository(Producto)
    private productoRepository: Repository<Producto>,
  ) {}

  crearProducto(producto: ProductoDTO): Promise<Producto> {
    return this.productoRepository.save({...producto,
      vendedor: {id: producto.vendedorId}
    });
  }

  listarProductos() {
    return this.productoRepository.find();
  }

  eliminarProducto(id: number) {
    return this.productoRepository.delete(id);
  }

  actualizarProducto(id: number, producto: ProductoDTO) {
    return this.productoRepository.update(id, producto);
  }
}
