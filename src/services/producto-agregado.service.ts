import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { ProductoAgregadoDTO } from 'src/dtos/producto-agregado.dto';
import { Repository } from 'typeorm';
import { ProductoAgregado } from '../entities/producto-agregado.entity';

@Injectable()
export class ProductoAgregadoService {
  constructor(
    @InjectRepository(ProductoAgregado)
    private productoAgregadoRepository: Repository<ProductoAgregado>,
  ) {}

  crearProductoAgregado(
    productoAgregado: ProductoAgregadoDTO,
  ): Promise<ProductoAgregado> {
    return this.productoAgregadoRepository.save({
      ...productoAgregado, 
      producto: {id: productoAgregado.productoId},
      compra: {id: productoAgregado.compraId}
    });
  }

  listarProductoAgregados() {
    return this.productoAgregadoRepository.find();
  }

  eliminarProductoAgregado(id: number) {
    return this.productoAgregadoRepository.delete(id);
  }

  actualizarProductoAgregado(
    id: number,
    productoAgregado: ProductoAgregadoDTO,
  ) {
    return this.productoAgregadoRepository.update(id, productoAgregado);
  }
}
